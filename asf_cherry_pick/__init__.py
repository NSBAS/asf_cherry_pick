
import importlib.metadata

__version__ = importlib.metadata.version("asf_cherry_pick")
